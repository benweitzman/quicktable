//
//  ViewModel.swift
//  QuickTableExample
//
//  Created by Ben Weitzman on 8/1/19.
//

import Foundation
import QuickTable

enum TestSection {
  case about, users(letter: String, items: [TestRow]), footer
}

extension TestSection: QuickTableSection {
  var items: [TestRow] {
    switch self {
    case .about: return [.about]
    case let .users(letter: _, items: items): return items
    case .footer: return [.footer]
    }
  }

  typealias Item = TestRow
}

enum TestRow {
  case about, user(String), footer
}

struct ViewModel {
  var data: [TestSection] {
    return [
      .about,
      .users(letter: "A", items: [
        .user("Anna")
        ]),
      .users(letter: "B", items: [
        .user("Ben")
        ]),
      .footer
    ]
  }
}
