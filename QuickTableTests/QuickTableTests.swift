//
//  QuickTableTests.swift
//  QuickTableTests
//
//  Created by Ben Weitzman on 7/31/19.
//

import XCTest
@testable import QuickTable

class QuickTableTests: XCTestCase {
  override func setUp() {
    // Put setup code here. This method is called before the invocation of each test method in the class.
  }

  override func tearDown() {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
  }

  var simpleTable = QuickTable<TestSection> { model in
    switch model {
    default: return Binder(UITableViewCell.self) { _ in }
    }
  }

  var customTable = QuickTable<TestSection> { model in
    switch model {
    case .rowA: return Binder(RowACell.self) { _ in }
    case .rowB: return Binder(RowBCell.self) { _ in }
    default: return Binder(UITableViewCell.self) { _ in }
    }
  }

  func testNoRows() {
    let tableView = UITableView()
    simpleTable.items = []
    XCTAssertEqual(simpleTable.numberOfSections(in: tableView), 0)
  }

  func testOneSectionOneRow() {
    let tableView = UITableView()
    simpleTable.items = [
      .one
    ]
    XCTAssertEqual(simpleTable.numberOfSections(in: tableView), 1)
    XCTAssertEqual(simpleTable.tableView(tableView, numberOfRowsInSection: 0), 1)
  }

  func testManySectionsManyRows() {
    let tableView = UITableView()
    simpleTable.items = [
      .one,
      .two([
        .rowB(0),
        .rowB(1),
        .rowB(2)
      ])
    ]

    XCTAssertEqual(simpleTable.numberOfSections(in: tableView), 2)
    XCTAssertEqual(simpleTable.tableView(tableView, numberOfRowsInSection: 0), 1)
    XCTAssertEqual(simpleTable.tableView(tableView, numberOfRowsInSection: 1), 3)
  }

  func testSimpleDeque() {
    let tableView = UITableView()

    simpleTable.setTableView(tableView)
    
    simpleTable.items = [
      .one
    ]

    let _ = tableView.cellForRow(at: IndexPath(row: 0, section: 0))!
  }

  func testCustomDeque() {
    let tableView = UITableView()

    customTable.setTableView(tableView)

    customTable.items = [
      .one,
      .two([.rowB(0)]),
      .three
    ]

    XCTAssertEqual(tableView.numberOfSections, 3)
    XCTAssertEqual(tableView.numberOfRows(inSection: 0), 1)
    XCTAssertEqual(tableView.numberOfRows(inSection: 1), 1)

    tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
    let cellA = tableView.cellForRow(at: IndexPath(row: 0, section: 0))!
    XCTAssertNotNil(cellA as? RowACell)

    tableView.scrollToRow(at: IndexPath(row: 0, section: 1), at: .top, animated: false)
    let cellB = tableView.cellForRow(at: IndexPath(row: 0, section: 1))!
    XCTAssertNotNil(cellB as? RowBCell)

    tableView.scrollToRow(at: IndexPath(row: 0, section: 2), at: .top, animated: false)
    let _ = tableView.cellForRow(at: IndexPath(row: 0, section: 2))!
  }
}

class RowACell: UITableViewCell { }
class RowBCell: UITableViewCell { }

enum TestSection: QuickTableSection {
  case one, two([TestRow]), three

  var items: [TestRow] {
    switch self {
    case .one: return [.rowA]
    case let .two(items): return items
    case .three: return [.rowC]
    }
  }
}

enum TestRow {
  case rowA, rowB(Int), rowC
}
